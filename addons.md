---
title: Addons
---

## uBlock Origin tweaks

[uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/) comes bundled with LibreWolf. Also see their [wiki](https://github.com/gorhill/uBlock/wiki). We'll mention a few filter lists and entry points to the uBlock wiki.

Some essential uBlock Wiki pages to understand how uBlock Origin works:

* Easy mode, the default mode that we ship with the browser -> [Blocking mode:easy-mode](https://github.com/gorhill/uBlock/wiki/Blocking-mode:-easy-mode).
* Dynamic filtering, suggested for enhanced protection -> [Dynamic filtering:quick guide](https://github.com/gorhill/uBlock/wiki/Dynamic-filtering:-quick-guide).
* Medium mode, suggested for enhanced protection, might require to fix some websites manually -> [Blocking mode:medium-mode](https://github.com/gorhill/uBlock/wiki/Blocking-mode:-medium-mode).


**Filter lists:**

- uBlock wiki: [Filter lists](https://github.com/gorhill/uBlock/wiki/Filter-lists-from-around-the-web): The uBlock wiki page on filter lists.
- [adblock nocoin list](https://github.com/hoshsadiq/adblock-nocoin-list): Good blocklist against crypto-mining.
- A collection of filter lists that are available on filterlists.com: [see this disussion page](https://github.com/DandelionSprout/adfilt).

We list two JavaScript blocking add-ons below, NoScript and uMatrix, but uBlock itself also has a JavaScript blocking option. More about that on this [wiki page](https://github.com/gorhill/uBlock/wiki/Per-site-switches#no-scripting).

## Recommended Addons

Recommended addons are not bundled and need to be installed manually, depending on your needs.

- [NoScript](https://addons.mozilla.org/en-US/firefox/addon/noscript/): NoScript Security Suite. Included into Tor Browser by default.
- [uMatrix](https://addons.mozilla.org/en-US/firefox/addon/umatrix/): Note: uMatrix development has frozen, but it does give _more control_ over what gets blocked than NoScript, either uMatrix or NoScript are good choices.
- [LocalCDN](https://addons.mozilla.org/en-US/firefox/addon/localcdn-fork-of-decentraleyes/): The successor to Decentraleyes.
- [Bitwarden Password Manager](https://addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager/).
- [KeepassXC-Browser](https://addons.mozilla.org/en-US/firefox/addon/keepassxc-browser/): Official browser plugin for the KeePassXC password manager

## Other Addons

### Privacy addons

- [ClearURLs](https://addons.mozilla.org/en-US/firefox/addon/clearurls/): Removes tracking elements from URLs.
- [Canvas Blocker](https://addons.mozilla.org/en-US/firefox/addon/canvasblocker/): Prevent some fingerprinting techniques.
- [Smart Referer](https://addons.mozilla.org/en-US/firefox/addon/smart-referer/): Manage referer with a button. (Send referers only when staying on the same domain.)

### Container addons

- [Mozilla's Multi Account Containers](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/): Manage containers and assign sites to specific container.
- [Temporary-Containers](https://addons.mozilla.org/en-US/firefox/addon/temporary-containers/): Maximizing and automating container potential.


### Other useful addons

- [Dark Reader](https://addons.mozilla.org/en-US/firefox/addon/darkreader/): Very popular addon, also useful for visually impaired people.

