---
title: Browser Tests
---

There are several good browser testing sites on the Internet. Here are just a few that we find most useful. We're always open for tips on new good ones.

## Security/Fingerprint

* The EFF has: [Cover Your Tracks](https://coveryourtracks.eff.org/).
* From arkenfox: [TorZillaPrint](https://arkenfox.github.io/TZP/index.html).
* [browserleaks.com](https://browserleaks.com/).
* [Device Info](https://www.deviceinfo.me/)
* Client side [SSLLabs](https://www.ssllabs.com/ssltest/viewMyClient.html)

## Performance

Performance tests can be done with [Octane](https://chromium.github.io/octane/). It needs to be launched with other applications closed and with no other activity. It is recommended to run the test multiple times and then calculate the average.

